import { useState, useEffect } from "react";
import ".//App.css";
import { TodoListData, TodoList } from "./TodoList/index";
import  defaultTodos from "./data";
import {TodoDataContext} from "./TodoContext/TodoContext";
import axios from 'axios';

const API_URL = "http://localhost:3001";

const App =()=> {
  const [todosData, setTodosData] = useState<TodoListData>(defaultTodos);
  
  useEffect(() => {
    axios.get(API_URL +'/todos')
      .then(response => setTodosData({todos: response.data}))
      .catch(error => console.error(error));
  }, []);

  return (
    <TodoDataContext.Provider value={{ todosData, setTodosData }}>
      <div className="App">
        <TodoList />
      </div>
    </TodoDataContext.Provider>
  );
}

export default App;
