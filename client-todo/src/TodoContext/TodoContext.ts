
import { createContext } from "react";
import { ContextTodoType } from "../TodoList/index";

const defaultContext: ContextTodoType = {
    todosData: {
      todos: [],
    },
    setTodosData: () => {},
  };

  export const TodoDataContext = createContext<ContextTodoType>(defaultContext);
