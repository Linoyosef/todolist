import { useContext } from "react";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import { TodoDataContext } from "../TodoContext/TodoContext";

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: "#9b59b6",
    color: theme.palette.common.white,
    "&:hover": {
      backgroundColor: "#63267b",
    },
    marginLeft: "456px",
  },
}));

export const TodoAddButton = () => {
  const classes = useStyles();
  const { todosData, setTodosData } = useContext(TodoDataContext);

  const newTodo = {
    id: todosData.todos.length + 1,
    title: "",
    isDone: false,
  };

  const handleClick = () => {
    setTodosData((prevTodos) => ({ todos: [newTodo, ...prevTodos.todos] }));
  };

  return (
    <Fab className={classes.button} aria-label="add" onClick={handleClick}>
      <AddIcon />
    </Fab>
  );
};
