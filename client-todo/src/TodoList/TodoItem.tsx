import { useContext, useState } from "react";
import { TextField } from "@material-ui/core";
import { TodoItemProps } from "./types";
import { TodoDataContext } from "../TodoContext/TodoContext";

const inputPropsStyle = {
  style: {
    color: "white",
    fontSize: "1.5rem",
    width: "36vw",
  },
};

export const TodoItem = ({ todo, onToggle }: TodoItemProps) => {
  const { setTodosData } = useContext(TodoDataContext);
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [editedTitle, setEditedTitle] = useState<string>(todo.title);

  const handleChange = () => {
    onToggle(todo.id);
  };

  const handleClickLabel = () => {
    setIsEditing(true);
  };

  const saveTodoItem = () => {
    setTodosData((prevTodosData) => {
      return {
        todos: prevTodosData.todos.map((item) => 
           item.id === todo.id ? { ...item, title: editedTitle } : item
        ),
      };
    });

    setIsEditing(false);
  };

  const handleEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      saveTodoItem();
    }
  };

  return (
    <div className="todoTitle">
      <input
        type="checkbox"
        className="todoCheckbox"
        checked={todo.isDone}
        onChange={handleChange}
      ></input>
      {isEditing ? (
        <TextField
          value={editedTitle}
          onBlur={saveTodoItem}
          onChange={(event) => setEditedTitle(event.target.value)}
          onKeyPress={handleEnter}
          InputProps={inputPropsStyle}
        />
      ) : (
        <label onClick={handleClickLabel}>
          {todo.title}
          <span
            className={todo.title === "" ? "transparent-click" : "initial"}
          />
        </label>
      )}
    </div>
  );
};
