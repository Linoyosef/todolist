import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TodoItem, TodoAddButton } from "./index";
import { TodoDataContext } from "../TodoContext/TodoContext";

const useStyles = makeStyles((theme) => ({
  AppHeader: {
    flexDirection: "column",
    alignItems: "center",
    fontSize: "80px",
    color: "white",
    paddingTop: "60px",
  },
  ItemList: {
    display: "grid",
    justifyContent: "center",
  },
  sortedTodosItems: {
    overflowY: "auto",
    height: "65vh",
    width: "55vw"
  },
}));

export const TodoList = () => {
  const classes = useStyles();
  const { todosData, setTodosData } = useContext(TodoDataContext);

  const handleToggle = (id: number) =>
    setTodosData((prevTodos) => ({
      todos: prevTodos.todos.map((todo) =>
        todo.id === id ? { ...todo, isDone: !todo.isDone } : todo
      ),
    }));

  const sortedTodosItems = todosData?.todos
    .sort((a, b) => (a.isDone === b.isDone ? 0 : a.isDone ? 1 : -1))
    .map((todo) => (
      <TodoItem key={todo.id} todo={todo} onToggle={handleToggle} />
    ));

  return (
    <>
      <div className={classes.AppHeader}> To Do List </div>
      <div className={classes.ItemList}>
        <ul className={classes.sortedTodosItems}>{sortedTodosItems}</ul>
      </div>
      <TodoAddButton />
    </>
  );
};
