import React, {SetStateAction} from "react";

export interface TodoProps {
    id: number,
    title : string,
    isDone : boolean
}

export interface TodoListData {
    todos: TodoProps[]
}

export interface ToDoListProps {
    todoListData: TodoListData
}
export interface TodoItemProps {
    todo: TodoProps;
    onToggle: (id: number) => void;
  }
  
export interface ContextTodoType {
    todosData: TodoListData,
    setTodosData: (React.Dispatch<SetStateAction<TodoListData>>)
  }