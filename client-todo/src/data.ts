
import { TodoListData } from "./TodoList/index";
let defaultTodos: TodoListData = {
  todos: []
}
 let todoListData: TodoListData = {
    todos: [
      {
        id: 1,
        title: "filter the todos that the is done false will be above",
        isDone: true,
      },
      {
        id: 2,
        title: "make the todos editable",
        isDone: true,
      },
      {
        id: 3,
        title: "adding a new to do",
        isDone: true,
      },
      {
        id: 4,
        title: "make a server - fetch",
        isDone: false,
      },
      {
        id: 5,
        title: "add material-ui classes",
        isDone: true,
      },
      {
        id: 6,
        title: "add todos checkbox hover style",
        isDone: true,
      },
      {
        id: 7,
        title: "adding mui classes",
        isDone: true,
      },
      {
        id: 8,
        title: "learning about sort 0 1 -1",
        isDone: true,
      },
      {
        id: 9,
        title: "add context",
        isDone: true,
      },
      {
        id: 10,
        title: "doing homework",
        isDone: true,
      },
      {
        id: 11,
        title: "doing homework",
        isDone: true,
      },
      {
        id: 12,
        title: "doing homework",
        isDone: true,
      },
      {
        id: 13,
        title: "doing homework",
        isDone: true,
      },
      {
        id: 14,
        title: "make a server -  edit",
        isDone: false,
      }, {
        id: 15,
        title: "make a server -  add",
        isDone: false,
      },
    ],
  };

  export default defaultTodos;