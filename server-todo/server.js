const express = require('express');
const { Pool } = require("pg");
const cors = require('cors');
const status = require('http-status');

const config  = {
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'qwe123',
  port: 5432,
};

const db = new Pool(config);

const app = express();

// Enable CORS for the client side
app.use(cors({
  origin: 'http://localhost:3000'
}));

app.use(express.json());

// Create a new todo item
app.post("/todos", async (req, res) => {
  const { title } = req.body;
  if (!title) {
     res.status(status.BAD_REQUEST).send("Title is required");
  }
  try {
    const queryResult = await db.query(
      "INSERT INTO todos.todos (title) VALUE $1 RETURNING *",
      [title]
    );
    const newTodo = queryResult.rows[0];
    res.status(status.CREATED).json(newTodo);
  } catch (err){
    console.log(err);
    res.status(status.INTERNAL_SERVER_ERROR).json({ massage:  "Server error - post"  });
  }
});

// Get all list
app.get("/todos", async (req, res) => {
  try {
    const queryResult = await db.query("SELECT * from todos.todos;");
    res.status(status.OK).json(queryResult.rows);
  } catch (err) {
    console.error(err);
    res.status(status.INTERNAL_SERVER_ERROR).json({ massage: "Server error - get!" });
  }
});

// Update todo
app.put("/todo/:id", async (req, res) => {
  const { id } = req.params;
  const { title, isDone } = req.body;
  try {
    const queryResult = await db.query(
      "UPDATE todos.todos SET title = $1, isDone = $2 WHERE id = $3",
      [title, isDone, id]
    );
    res.status(status.OK)
  } catch (err) {
    console.log(err)
    res.status(status.INTERNAL_SERVER_ERROR).json({ massage: "Server error -  put" });
  }
});

const port = 3001;
app.listen(port, () => console.log(`Listening on port ${port}`));
